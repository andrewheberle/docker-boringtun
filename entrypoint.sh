#!/bin/sh

set -e

finish() {
    wg-quick down "${WG_INTERFACE}"
    exit 0
}
trap finish TERM INT QUIT

wg-quick up "${WG_INTERFACE}"

# Inifinite sleep
while true; do
    sleep 86400
done
