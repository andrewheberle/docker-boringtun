BUILD_TAG := $(shell git describe --long --tags)
BUILD_IMAGE_NAME = $(REGISTRY)/$(IMAGE_NAME):$(BUILD_TAG)

# Image tags
BUILD_TAG_BARE := $(shell echo $(BUILD_TAG) | cut -d- -f1)
MAJOR_VERSION := $(shell echo $(BUILD_TAG_BARE) | cut -d. -f1)
MINOR_VERSION := $(shell echo $(BUILD_TAG_BARE) | cut -d. -f2)
PATCH_VERSION := $(shell echo $(BUILD_TAG_BARE) | cut -d. -f3)
FULL_VERSION := $(shell git describe --tags)
MAJOR_TAG = $(REGISTRY)/$(IMAGE_NAME):$(MAJOR_VERSION)
MINOR_TAG = $(REGISTRY)/$(IMAGE_NAME):$(MAJOR_VERSION).$(MINOR_VERSION)
FULL_TAG = $(REGISTRY)/$(IMAGE_NAME):$(MAJOR_VERSION).$(MINOR_VERSION).$(PATCH_VERSION)
FULL_VERSION_TAG = $(REGISTRY)/$(IMAGE_NAME):$(FULL_VERSION)
LATEST = $(REGISTRY)/$(IMAGE_NAME):latest

# Build replacement command for Dockerfile
SED_REPLACE = $(foreach var,$(DOCKERFILE_REPLACE_VARS),-e 's:@$(var)@:$($(var)):')

# optionally include any extra vars
-include vars.mk

.PHONY: check-dockerfile-env check-env build tag release clean help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

Dockerfile: check-dockerfile-env Dockerfile.in ## Generate Dockerfile.
	sed $(SED_REPLACE) Dockerfile.in > $@

build: check-env Dockerfile ## Build image.
	docker build $(DOCKER_BUILD_OPTS) -t $(BUILD_IMAGE_NAME) .

tag: build ## Build and tag images.
	docker tag $(BUILD_IMAGE_NAME) $(MAJOR_TAG)
	docker tag $(BUILD_IMAGE_NAME) $(MINOR_TAG)
	docker tag $(BUILD_IMAGE_NAME) $(FULL_TAG)
	docker tag $(BUILD_IMAGE_NAME) $(LATEST)

release: check-release-env tag ## Build, tag and push images.
	docker push $(MAJOR_TAG)
	docker push $(MINOR_TAG)
	docker push $(FULL_TAG)
	docker push $(LATEST)

check-dockerfile-env:
ifndef BASE_IMAGE
	$(error BASE_IMAGE is not defined)
endif

ifndef BASE_TAG
	$(error BASE_TAG is not defined)
endif

ifndef BUILDER_IMAGE
	$(error BUILDER_IMAGE is not defined)
endif

ifndef BUILDER_TAG
	$(error BUILDER_TAG is not defined)
endif

check-env:
ifndef BUILD_TAG
	$(error BUILD_TAG is not defined)
endif

check-release-env:
ifndef REGISTRY
	$(error REGISTRY is not defined)
endif

clean:
	$(RM) Dockerfile
