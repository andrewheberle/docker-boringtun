# boringtun docker

## Setup

First of all you need a key pair for the server. Use the following command to generate the public and private keys:

```sh
# Create location for config and files
mkdir ./boringtun
cd ./boringtun

# Generate privatekey
docker run --rm -i registry.gitlab.com/andrewheberle/docker-boringtun wg genkey > privatekey

# Generate publickey from privatekey
docker run --rm -i registry.gitlab.com/andrewheberle/docker-boringtun wg genkey < privatekey > publickey

# Generate config
cat > wg0.conf
[Interface]
# Assign you an IP (that's not in use) and add it to server configmap
Address = 10.33.0.2/32
# generate private key using `wg genkey`
PrivateKey = <your private key>

[Peer]
# Wireguard server public key
PublicKey = AbC...XyZ=
Endpoint = 1.2.3.4:51820
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 25
EOF
```

## Running

### Docker

```sh
docker run --cap-add NET_ADMIN -ti --rm -v /dev/net/tun:/dev/net/tun -v ./boringtun:/etc/wireguard registry.gitlab.com/andrewheberle/docker-boringtun
```

### Docker Compose

```yaml
version: '3.3'
services:
  wireguard:
    image: registry.gitlab.com/andrewheberle/docker-boringtun
    cap_add:
     - NET_ADMIN
    sysctls:
     - net.ipv4.ip_forward=1
    volumes:
     # Folder with 'publickey', 'privatekey' and 'wg0.conf'
     - ./boringtun:/etc/wireguard
     - /dev/net/tun:/dev/net/tun
    ports:
     - 51820:51820/udp
    # Uncomment the following line when 'AllowedIPs' is '0.0.0.0/0'
    # privileged: true
    restart: always
```

```sh
docker-compose up -d
```
