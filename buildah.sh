#!/bin/sh

set -e
# Variables
BASE_VERSION="3.12"
BUILDER_VERSION="alpine${BASE_VERSION}"
WG_TOOLS_TAG="v1.0.20200827"
BORINGTUN_TAG="v0.3.0"

# Builder image
build=$(buildah from rust:${BUILDER_VERSION})

# Env vars for build
buildah config --env WITH_WGQUICK=yes ${build}

# Build pre-reqs
buildah run ${build} -- apk --no-cache add git build-base libmnl-dev iptables

# Build wiregiuard-tools
buildah run ${build} -- git clone https://git.zx2c4.com/wireguard-tools && \
    cd wireguard-tools && \
    git checkout ${WG_TOOLS_TAG} && \
    cd src && \
    make && \
    make install

# Build boringtun
buildah run ${build} -- git clone https://github.com/cloudflare/boringtun && \
    cd boringtun && \
    git checkout ${BORINGTUN_TAG} && \
    cargo build --bin boringtun --release && \
    mv ./target/release/boringtun /usr/bin/boringtun

# Main image
image=$(buildah from alpine:${BASE_VERSION})

# Main pre-reqs
buildah run ${image} -- apk add --no-cache bash libmnl iptables openresolv iproute2

# Set cmd
buildah copy ${image} entrypoint.sh /
buildah run ${image} -- chmod +x /entrypoint.sh
buildah config --cmd /entrypoint.sh ${image}

# Copy contents from build container
mnt=$(buildah mount ${build})
buildah copy ${image} ${mnt}/usr/bin/boringtun /usr/bin/boringtun
buildah copy ${image} ${mnt}/usr/bin/wg /usr/bin/wg
buildah copy ${image} ${mnt}/usr/bin/wg-quick /usr/bin/wg-quick
buildah umount ${image}
  
# Environment
buildah config --env WG_QUICK_USERSPACE_IMPLEMENTATION="boringtun" ${image}
buildah config --env WG_INTERFACE="wg0" ${image}
buildah config --env WG_SUDO="1" ${image}
buildah config --env WG_COLOR_MODE="always" ${image}
