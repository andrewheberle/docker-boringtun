# Image config
BASE_IMAGE = alpine
BASE_TAG = 3.13
BUILDER_IMAGE = rust
BUILDER_TAG = $(BASE_IMAGE)$(BASE_TAG)

# Wireguard and Boringtun versions
WG_TOOLS_TAG = v1.0.20210315
BORINGTUN_TAG = v0.3.0

# Registry path for tagging image
REGISTRY = registry.gitlab.com/andrewheberle

# Image information/metadata
IMAGE_AUTHOR = Andrew Heberle
IMAGE_DESCRIPTION = WireGuard Container utilising boringtun
IMAGE_NAME = docker-boringtun
IMAGE_VCS_BASE_URL = gitlab.com/andrewheberle

# Additional docker build options
DOCKER_BUILD_OPTS = --pull

# Values to replace in the Dockerfile
DOCKERFILE_REPLACE_VARS = BUILDER_IMAGE BUILDER_TAG BASE_IMAGE BASE_TAG WG_TOOLS_TAG BORINGTUN_TAG IMAGE_AUTHOR IMAGE_DESCRIPTION IMAGE_NAME IMAGE_VCS_BASE_URL
